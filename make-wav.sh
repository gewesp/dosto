#! /bin/bash

DIR=./measurements/c1

./analysis/analyze.py --freq 48000 --speedup 100 --analysis wav --file ./measurements/c1/win-flg-ic2k-1.tsv
./analysis/analyze.py --freq 48000 --speedup 100 --analysis wav --file ./measurements/c1/win-flg-bomb-1.tsv

mv -v $DIR/*.wav results/wav
