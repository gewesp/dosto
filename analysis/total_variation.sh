#! /bin/bash

for i in $* ; do
  ./analyze.py --file "$i" --analysis total_variation
done
