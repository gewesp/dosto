#! /usr/bin/python3
#
# Produces plot from an acceleration measurement, AccDataRec format
#
# If --analysis is wav, writes scaled data to a .wav file with the same file name base
# Use --speedup to actually hear something meaningful (the frequencies we're interested
# in are in the 1Hz order of magnitude, so speedup values of 100 make sense).
#
# TODO 
# * Accept non-option arguments and iterate over them instead of --file
#

import argparse
import math
import sys

import numpy as np
import matplotlib.pyplot as plt
import os.path
import scipy.io.wavfile

from numpy import linalg
from scipy import interpolate
from scipy import signal

# Default scales for acceleration values [m/s^2]
# x: lateral
# y: longitudinal (in direction of travel)
# z: vertical
ACCELERATION_SCALE = {
    'x': {'min': -2, 'max': 2},
    'y': {'min': -2, 'max': 2},
    'z': {'min': 7, 'max': 11}
}

def basename(args):
    """
    For dir/ectory/file.extension, returns dir/ectory/file .
    """
    return os.path.splitext(args.file)[0]

def basebasename(args):
    """
    For measurements/xxx-yy-z.tsv, returns xxx-yy-z;
    You get the picture.
    """
    return os.path.splitext(os.path.basename(args.file))[0]

def write_wav(data, args):
    """
    Writes data as a mono 16bit .wav file.
    Data is assumed to be scaled to [-1, 1] and evenly spaced on the time
    axis.
    Uses Writes to args.file with any extension replaced by .wav.
    """
    # Assert some reasonable sampling frequency
    if args.freq < 8000:
        raise ValueError(
            'Writing .wav file: Sampling frequency (--freq) too low (need at least 8000)')
    # Guesswork, see https://stackoverflow.com/questions/18645544/writing-wav-file-in-python-with-wavfile-write-from-scipy
    d2 = np.asarray(30000 * data, dtype=np.int16)
    wavname = basename(args) + '.wav'
    print('Writing wav file:', wavname)
    print(d2)
    scipy.io.wavfile.write(wavname, args.freq, d2)

def normalize_data(data, args):
    """
    Scales and centers data according to args, or such that
    minval maps to -1, maxval maps to 1
    Also, clips (clamps) value to this range.
    TODO: Allow override in args (?)
    """
    sc = ACCELERATION_SCALE[args.which]

    minval = sc['min']
    maxval = sc['max']
    span = maxval - minval

    return np.clip(-1.0 + 2.0 * (data - minval) / span, -1, 1)

def default_arguments(prog):
    """
    Defines some standard command line arguments.
    :param pars: Return value of argparse.ArgumentParser()
    """
    pars = argparse.ArgumentParser(prog=prog)
    pars.add_argument('--file', nargs='?', help='The data file (TSV format from AccDataRec)',
                      default='measurements/win-flg-ic2k-1.tsv')
    pars.add_argument('--format', help='Input data file format',
                      choices=['accdatarec', 'vibsensor'],
                      default='accdatarec')
    pars.add_argument('--out_format', help='Output file format for plots',
                      choices=['pdf', 'png'],
                      default='png')
    pars.add_argument('--which', nargs='?', 
                      help='Which acceleration to analyze (x, y, z), time (t), time diff (dt)',
                      default='x')
    pars.add_argument('--analysis', nargs='?',
                      help="Analysis to perform: time ('time') or frequency ('spec') domain plot, write .wav ('wav') or 'total_variation'",
                      default='time')
    pars.add_argument('--interpolation', nargs='?',
                      help='Interpolation: linear, spline',
                      default='linear')
    # pars.add_argument('--plot_scaled',
    #                   action='store_true',
    #                   help='Whether or not plot scaled values in time domain plot')
    pars.add_argument('--freq', nargs='?',
                      help="Sample frequency if resampling and writing to .wav is desired)",
                      default=100,
                      type=int)
    pars.add_argument('--speedup', nargs='?',
                      help="Speedup factor, only relevant if resampling happens",
                      default=1.0,
                      type=float)
    pars.add_argument('--t_min', nargs='?',
                      help="Begin time e.g. for total variation calculation",
                      default=-1e300,
                      type=float)
    pars.add_argument('--t_max', nargs='?',
                      help="End time e.g. for total variation calculation",
                      default=1e300,
                      type=float)
    return pars.parse_args()


def read_vibsensor(args):
    """
    Reads a VibSensor file

    Header line: time (sec),X raw (m/s^2),Y raw (m/s^2),Z raw (m/s^2)
    Format: <timestamp>,<xacc>,<yacc>,<zacc>
    E.g.: 0.120118,-0.468066,0.6608,9.74201

    Some files have:
    Header line:
    time (sec),X raw (m/s^2),Y raw (m/s^2),Z raw (m/s^2),Start Time,Start Date,Duration (sec),Data Rate (Hz)
    The first data line has the header info.  These are edited to accomodate all 
    info in the first line.
    Timestamps are in [s] relative to start of measurement.

    :return: Dict with entries 't', 'xacc', 'yacc', 'zacc' in SI units
    """
    csv = np.genfromtxt(args.file, delimiter=',', skip_header=1)
        
    t = csv.T[0]
    if not isinstance(t, np.ndarray):
        raise RuntimeError("Couldn't read VibSensor file; try --format option?")
    return {'t': t, 'xacc': csv.T[1], 'yacc': csv.T[2], 'zacc': csv.T[3]}

def read_accdatarec(args):
    """
    Reads an AccDataRec file

    Header line: None
    Format: <sequence_number> <timestamp_ms> <xacc> <yacc> <zacc> <filename>
    <filename> is always the same and ignored.
    E.g.: 2       1552386280341   0.118   1.401   9.699   win-flg-ir13-sie

    Timestamps are in [ms] relative to Jan 1, 1970.

    :return: Dict with entries 't', 'xacc', 'yacc', 'zacc' in SI units
    """
    tsv = np.genfromtxt(args.file, delimiter='\t')
    t_ms = tsv.T[1]
    if not isinstance(t_ms, np.ndarray):
        raise RuntimeError("Couldn't read AccDataRec file; try --format option?")
    t0 = t_ms[0]
    t = 1e-3 * (t_ms - t0)
    return {'t': t, 'xacc': tsv.T[2], 'yacc': tsv.T[3], 'zacc': tsv.T[4]}
    
def scale_time(t, data, args):
    """
    Resample time series (t, data) with given args.speedup and
    sampling frequency args.freq, using piecewise linear
    interpolation.
    :param ip: Which interpolation (linear, spline)
    :return: x and y values, where x values are evenly spaced with 1/freq
    """
    # t0 should be 0
    t0 = t[0]  / args.speedup
    t1 = t[-1] / args.speedup

    # Creates PL interpolation function object
    tt = np.arange(t0, t1, 1.0 / args.freq)

    if 'linear' == args.interpolation:
      f = interpolate.interp1d(t, data, axis=0)
      # Transform back to original time for actual speedup to happen
      return (tt, f(args.speedup * tt))
    elif 'spline' == args.interpolation:
      # Cubic spline (recommended in scipy doc)
      spl = interpolate.splrep(t, data, k=3)
      return (tt, interpolate.splev(args.speedup * tt, spl))
    else:
      raise AssertionError('Unknown interpolation method: ' + args.interpolation)

def total_variation(t, y, args):
    """
    :return: The total variation of y, optionally restricted to args.t_min and
    args.t_max
    """
    assert len(t) == len(y)
    # np.nonzero returns array indices
    yy = y[np.nonzero(np.logical_and(t >= args.t_min, t <= args.t_max))]
    return np.sum(np.abs(np.diff(yy)))

def acc_which(data, args):
    """
    Selects the acceleration column we want (x/y/z)
    """
    return data[args.which + 'acc']

def acc_wave(data, args):
    """
    Writes acceleration data as .wav file after resampling
    and speedup according to args.
    """
    if not args.which in ['x', 'y', 'z']:
        raise ValueError('Can only convert accelerations to .wav (--which argument)')

    t = data['t']
    # Y normalized to acceleration ranges
    y_norm = normalize_data(acc_which(data, args), args)
    # ys: y at regular time intervals
    (_, ys) = scale_time(t, y_norm, args)
    write_wav(ys, args)

def tot_var(data, args):
    t = data['t']
    y = acc_which(data, args)

    print('Total variation of {}: {:.1f} m/s^2'.format(args.file, total_variation(t, y, args)))

def acc_plot(data, args):
    """
    Time/frequency domain plot of acceleration data according
    to args.
    """
    # print(data['t'])
    t = data['t']

    fig = plt.figure()

    # Special plots: Time and dt, just for verification
    if 't' == args.which:
        print('Plotting time scale...')
        plt.plot(t)
        plt.xlabel('Number of samples')
        plt.ylabel('Time [s]')
        plt.title(basebasename(args) + '/Time')
    elif 'dt' == args.which:
        print('Plotting time scale...')
        plt.xlabel('Number of samples')
        plt.ylabel('dTime [s]')
        plt.title(basebasename(args) + '/dTime')
        plt.plot(np.diff(t))

    else:
        y = acc_which(data, args)

        if 'time' == args.analysis:
            print('Plotting in time domain...')
            plt.plot(t, y)
            plt.ylabel('Acceleration [m/s^2]')
            sc = ACCELERATION_SCALE[args.which]
            plt.ylim(sc['min'], sc['max'])
        elif 'spec' == args.analysis or 'spectrogram' == args.analysis:
            print('Plotting in frequency domain...')
            if args.freq < 0:
                raise AssertionError(
                    'Must specify sampling frequency (--freq) for spectrogram')
            if not args.which in ['x', 'y', 'z']:
                raise AssertionError(
                    'Spectrogram plot only possibly for x, y or z acceleration (--which parameter)')
            # Do not normalize---we rely on detrending below
            # ys: y at regular time intervals
            (_, ys) = scale_time(t, y, args)
            f, t, Sxx = signal.spectrogram(
                ys, nperseg=256, noverlap=0, detrend='linear', fs=args.freq)
            plt.pcolormesh(t, f, 10*np.log10(Sxx), shading='gouraud', vmin=-70, vmax=-30)
            plt.ylabel('Frequency [Hz]')

            print('Length:', len(ys))
            #Pxx, freqs, bins, im = plt.specgram(
            #    ys, NFFT=256, noverlap=128, Fs=args.freq, detrend='linear', scale='dB')
            # print('Frequencies:', freqs)
            # plt.ylim(0, 10000)
        else:
            raise AssertionError(
                'Unknown analysis (--analysis): ' + args.analysis)

        plt.xlabel('Time [s]')
        plt.title(basebasename(args) + '/axis ' + args.which + '/' + args.format)

    plt.show()

    fig.savefig(basename(args) + '-' + args.which + '.' + args.out_format,
        bbox_inches='tight')


try:
    args = default_arguments('analyze.py')

    if 'accdatarec' == args.format:
        data = read_accdatarec(args)
    elif 'vibsensor' == args.format:
        data = read_vibsensor(args)
    else:
        raise ValueError('Unknown format: ' + args.format)

    if 'wav' == args.analysis:
        acc_wave(data, args)
    elif 'total_variation' == args.analysis:
        tot_var(data, args)
    else:
        acc_plot(data, args)

    sys.exit(0)
except Exception as e:
    print('ERROR:', e)
    sys.exit(1)
