# Vergleich FV-DOSTO mit IC2000

* Autor: Gerhard Wesp, [KISS Technologies GmbH](https://www.kisstech.ch/), Adliswil
* Erstellt: 29.3.2019
* Geändert: 11.8.2021

![text](./png/frequency/win-flg-bomb-1-x.png "Winterthur-Flughafen FV-DOSTO")

Dem neuen Fernverkehrs-Doppelstockzug (FV-DOSTO) der SBB wird
nachgesagt, dass es speziell im Obergeschoss mehr schüttelt und dass
Fahrgästen teilweise sogar übel wird.

Im März 2019 haben wir Messungen in Fahrten zwischen Zürich Flughafen
und Winterthur im IC2000 (die klassischen IC-Züge auf der Hauptstrecke
St. Gallen-Genf) und dem neuen FV-DOSTO gemacht.  Ziel war, die
subjektiven Aussagen, welche in der Presse und Onlineforen
herumgeistern, mit Fakten zu untermauern oder zu widerlegen.

Das Messinstrument ist ein Mobiltelefon Samsung Galaxy S4 Active mit
einem STMicroelectronics-Beschleunigungsmesser.  Dieser wurde zuerst im
Stillstand beobachtet, um das Messrauschen abzuschätzen.  Dies ist im
Bereich von 0.05 m/s^2, genau genug, um Werte im Bereich bis 2 m/s^2 zu
messen.

Die fahrplanmässige Fahrtdauer ist 12 Minuten.  Die Messung startet
jeweils kurz vor der Abfahrt, sodass etwa 750s gemessen wurde.
100 Mal pro Sekunde werden Beschleunigungswerte aufgezeichnet.  Dies
erlaubt uns, Frequenzen bis in den unteren hörbaren Bereich zu
analysieren.  Betrachtet wird hier die Transversalbeschleunigung
(quer zur Fahrtrichtung), welche die grösste Variation zeigt.

Wir machten pro Richtung je eine Fahrt auf IC2000 (Linien IC1 und IR75)
und FV-DOSTO (Linien IR13 und IR37).


### FV-DOSTO schüttelt doppelt so stark

Die Resultate sind klar:  Der FV-DOSTO schüttelt tatsächlich etwa
doppelt so stark wie der IC2000.  Quantifiziert kann das mit der
sogenannten
[Totalen Variation](https://de.wikipedia.org/wiki/Variation_(Mathematik))
der Beschleunigung.  Diese ist ein Mass für die
Änderung einer Grösse über eine gewisse Zeit (z.B. die Dauer der
Zugfahrt) und damit für das Schütteln.

In der Folge sehen wir uns die zwei Fahrten von Winterthur
nach Flughafen an.  Die Messungen in die Gegenrichtung sind
im git-Repository enthalten und qualitativ sehr ähnlich.


## Zeitbereich

Bereits ein erster Blick auf die Plots zeigt offensichtliche
Unterschiede.

#### Winterthur-Flughafen im IC2000

![text](./png/time/win-flg-ic2k-1-x.png "Winterthur-Flughafen IC2000")

#### Winterthur-Flughafen im FV-DOSTO

![text](./png/time/win-flg-bomb-1-x.png "Winterthur-Flughafen FV-DOSTO")


### Fahrabschnitte

Sehen wir uns die Einfahrt im Flughafen im Detail an.
Hier wird deutlich, dass die Oszillationen im FV-DOSTO mindestens
doppelt so gross sind wie im IC2000.

#### Einfahrt Flugafen im IC2000

![text](./png/einfahrt-flg-ic2k.png)

#### Einfahrt Flugafen im FV-DOSTO

![text](./png/einfahrt-flg-bomb.png)


## Frequenzbereich

Wir haben uns auch für die Schwingungsfrequenzen interessiert.
Interessant ist vor allem der Bereich zwischen 1Hz und 20Hz,
denn was darüber liegt, wäre bereits hörbar.

#### Winterthur-Flughafen im IC2000

![text](./png/frequency/win-flg-ic2k-1-x.png "Winterthur-Flughafen IC2000")


#### Winterthur-Flughafen im FV-DOSTO

![text](./png/frequency/win-flg-bomb-1-x.png "Winterthur-Flughafen FV-DOSTO")


## Audio

Die Analysesoftware erlaubt es, die Beschleunigungsmessung in eine
Audiodatei umzuwandeln, wobei die Zeit schneller läuft um das
Schütteln des Zuges hörbar zu machen.

* [So hört sich der IC2000 an](./wav/win-flg-ic2k-1.wav)
* [Und so der FV-DOSTO](./wav/win-flg-bomb-1.wav)

Hier läuft die Zeit 160 mal schneller, sodass sich die 'Fahrzeit' auf
etwa 4s verkürzt.

Man hört, dass der IC2000 deutlich 'leiser' ist als der FV-DOSTO.  Dies
entspricht den ruhigeren Bewegungen des Zuges.
Beim FV-DOSTO hört man deutlich die Töne, die im Frequenzplot bei
ca. 12Hz und 24Hz sichtbar sind.

# Ausblick

Eine Liste möglicher zukünftiger Fragestellungen:
* Messungen anderer Doppelstockkompositionen wie Stadler KISS, Siemens
* Messung der ICN-Kompositionen (diese haben auch einen Ruf als 'trains vomitaux')
* Messungen im IC2000 und FV-DOSTO im unteren Stockwerk
* Woher stammen die 12Hz-Schwingungen im FV-DOSTO?
* Welchen Einfluss hat die FV-DOSTO Wankkompensation?
* Messungen auf anderen (längeren) Strecken
