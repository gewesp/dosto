# Dosto

Dosto is a tool to analyse acceleration time series.

It was originally developed to analyse accelerations the passengers
are exposed to in [Swiss trains](http://www.sbb.ch).
The word 'Dosto' is a German shorthand for 'Doppelstock' which
means double-decker, especially for trains.

Accelerations are measured with COTS mobile phones.

# Results

See the [Comparison of IC2000 with FV-DOSTO](results/ic2000-fv-dosto.md)
which confirms that the FV-DOSTO is about twice as 'shaky' as the
IC2000.

Pull requests with other comparisons are more than welcome!


# Quick intro

## Prerequisites

* You'll need python3, matplotlib and scipy.  
* Installation on Ubuntu: using `apt install python3-matplotlib python3-scipy`


## Analysing accelerations

* `./analysis/analyze.py`
Time domain plot of X (lateral) acceleration of a default measurement file

* `./analysis/analyze.py --file measurements/<file>.tsv`
Time domain plot of X (lateral) acceleration of another measurement

* `./analysis/analyze.py --analysis spec --file measurements/<file>.tsv`
Frequency domain plot of X (lateral) acceleration

* `./analysis/analyze.py --analysis spec --which z --file measurements/<file>.tsv`
Frequency domain plot of Z (vertical) acceleration

* `./analysis/analyze.py --freq 48000 --speedup 100 --analysis wav --file measurements/<file>.tsv`
Converts measurement to audio file (.wav format) with given sampling frequency
and time sped up by the given factor.  Makes oscillations audible.

# HOWTO measure accelerations

* To measure on a train: Place phone on the middle of a seat. 
  Display should face up, top of phone point into direction of travel.

## Android

* Set Settings -> Display -> Screen timeout to 30 minutes
* Do *not* put the phone in sleep mode.  The app apparently doesn't
  work in the background.  You can reduce screen brightness to save
  power.
* Install VibSensor (ca. CHF 5.--, but much better timing behavior)
  or AccDataRec (free)
* On some phones, dialing `*#0*#` will show sensor information.

### VibSensor

* Install VibSensor from the 'Play store'
* Start the app
* Select 'Settings' in the lower tool bar
* Activate the 'Email' feature (cost: ca. CHF 5.--; actually it allows
  sharing data using arbitrary methods, not just email)
* Ensure 'Frequency Range' is set to 'high'
* Select 'Acquire' and maximum duration (10 minutes) 
* Press Start

### Sharing VibSensor measurements

* Select 'ViewData' in the app
* Select the run you want to upload
* Select 'raw' (menu bar in middle of screen)
* Select the 'upload' symbol in the bottom tool bar
* Share the file(s) via email, Google Drive, whatsapp etc.

## How to take measurements with AccDataRec (Android)

_NOTE:_ AccDataRec has bad jitter in the sample times which
may interfere with measurements.

* Install AccDataRec from the 'Play store'
* Start the app
* Press CAPTURE
* Enter a file name, preferably according to the naming convention in
  the `measurements/` subdirectory.
* Note the Accelerometer sensor name from the Message Console (can
  also be done later)
* **Now**, place phone on the middle of a seat. Display should face up,
  top of phone point into direction of travel.
* Press START RECORDING
* Select 'Fastest' acquisition speed
* WARNING: Duplicate filenames caues the app to attach a timestamp
  to the *file name*.

### Sharing AccDataRec measurements

* Open the 'My Files' app on your phone.
* Go to All Files -> AccDataRec. 
* Select the checkbox on the left of the file(s) you want to upload.
* Select the 'Share' symbol on the top toolbar
* Share the file(s) via email, Google Drive, whatsapp etc.

Pull requests with measurements are welcome!


## iPhone

VibSensor also exists for iPhone.  Please apply the instructions for
Android above and inform us about any necessary changes
in the instructions.


## Keuwlsoft 

* "Accelerometer meter": Do not use; cannot save more than 200s in 'fast' (100Hz) mode


# Test phones

Engineer | Phone short | Phone long | Acceleration sensor
--- | --- | --- | ---
GEW | GALS4A | Samsung Galaxy S4 Active | STMicroelectronics K330
GEW | IPHO7 | iPhone 7 | TBD
CED |        | TBD | K2HH Acceleration (STM)

# Manifest


## Measurements

* `measurements/`: Data recorded during train rides
* `measurements/test/`: Test measurements, e.g. standstill


## Python code

* `analysis/`:  Python/matplotlib code to produce time and frequency domain 
  graphs 

# Measurements


## Parameters

Unless mentioned otherwise, the parameters are:
* Sensor placement:  Top floor, middle of car. Phone lying in middle of seat,
  top of phone in direction of travel and display facing upwards.
* Hardware: Samsung Galaxy S4 active with STMicroelectronics K330 
  3-axis accelerometer version 1
* Nominal acquisition frequency: 100Hz
* Tracks according to timetable
* 2nd class cars/compartments


## Train types

Shorthand | Description          
---       | ---
DPZ       | SBB-Doppelstock-Pendelzug DPZ + Re 450
BOMB      | Bombardier Twindexx Swiss Express (SBB: FV-DOSTO)
KISS      | Stadler KISS (formerly Stadler DOSTO)
IC2K      | Schindler Wagon IC2000


## Stadelhofen/Uster

* Sensor: GALS4A; Top floor, various seats
* Engineer: GEW

Parameter             | Run 1                           | Run 2
---                   | ---                             | ---
File                  | sta-ust-kiss-1.tsv              | ust-sta-kiss-1.tsv
Date                  | FRI MAR 15, 2019                | FRI MAR 15, 2019
Note                  |                                 | 
Train line            | S5                              | S5
Train type            | KISS                            | KISS
Departure             | Stadelhofen 06:27 T3            | Uster 17:21 T1
Arrival               | Uster 6:38 T2                   | Stadelhofen 17:32 T1
Car                   | 3/6; set 1/1                    | 3/6; set 3/3

## Oerlikon/Hardbruecke

* Sensor placement: Top floor, window seat
* Test measurement with IPHO7
* Directory: `misc`

Parameter             | Run 1                
---                   | ---                 
File                  | `Raw2019-04-06(203934).csv`
Date                  | SAT APR 6, 2019
Note                  |                  
Train line            | S9
Train type            | DPZ
Departure             | 
Arrival               |
Car                   |

## Campaign 1: Flughafen/Winterthur

* Dates: WED MAR 20, 2019 and THU MAR 21, 2019
* Sensor: GALS4A; Top floor, phone lying on seats; usually window seat
* Engineer: GEW
* Directory: `c1`

Parameter      | Run 1                 | Run 2                | Run 3              | Run 4
---            | ---                   | ---                  | ---                | ---
File           | flg-win-bomb-1.tsv    | win-flg-bomb-1.tsv   | flg-win-ic2k-1.tsv | win-flg-ic2k-1.tsv
Date           | WED MAR 20, 2019      | THU MAR 21, 2019     | THU MAR 21, 2019   | THU MAR 21, 2019
Note           |                       |                      |                    | 
Train line     | IR37                  | IR13                 | IC1                | IR75
Train type     | BOMB                  | BOMB                 | IC2K               | IC2K
Departure      | Flughafen  19:14 T1   | Winterthur 07:24 T3  | Flughafen 07:44 T1 | Winterthur 08:01 T4
Arrival        | Winterthur 19:27 T5   | Flughafen 07:36 T3   | Winterthur 07:57 T5| Flughafen 08:13 T3
Car            |                       |                      |                    |                             

## Campaign 2: Flughafen/Winterthur

* Dates: TUE APR 9, 2019
* Sensors: GALS4A/ACDR, IPHO7/VIBS
* Sensor location: Top floor; affixed to table using Tesa Power strips
* Engineer: GEW
* Directory: `c2`

Time | From | To  | Train/Type | File GALS4A | File IPHO7
---  | ---  | --- | ---        | ---         | ---
08:46| FLG  | WIN | IR75/IC2K  | flg-win-ic2k-1.tsv | -
10:24| WIN  | FLG | IR13/BOMB  | win-flg-bomb-1.tsv | win-flg-bomb-1.csv
11:23| FLG  | WIN | IR13/BOMB  | flg-win-bomb-1.tsv | flg-win-bomb-1.csv
12:31| WIN  | FLG | IC8/IC2K   | win-flg-ic2k-1.tsv | win-flg-ic2k-1.csv

* Notes: Took a KISS train once, not listed above but measurement in
  directory.


## Zug/Thalwil

Sensor: TBD; Top floor
Engineer: CED
Directory: Misc

Parameter      | Run 1
---            | ---
File           | zug-thw-ic2k-1.tsv
Date           | SUN MAR 31, 2019
Note           |
Train line     |
Train type     | IC2K
Departure      |
Arrival        |
Car            |

## Standstill validation

Measurements of 1 minute to validate results at standstill.

Phones are placed on a firm horizontal surface, screen facing up. 

File | Phone type | App
---  | ---        | ---
`standstill-gals4a-acdr.tsv` | GALS4A | AccDataRec
`Raw2019-03-29(12_08_41).csv` | GALS4A | VibSensor
`Raw2019-04-06(181533).csv` | IPHO7 | VibSensor

### VibSensor vs. AccDataRec during validation (GALS4A)

* Acceleration values: X values are a bit higher in VibSensor (average 0.22 m/s^2)
  than in AccDataRec (average 0.155 m/s^2).  Measurement noise is very similar
  (ca. 0.05 m/s^2).
* Timing jitter (`analyze.py --which dt ...`):
  * VibSensor: dt values between 0.01 and 0.0101 (reasonable)
  * AccDataRec: dt values between 0.001 and 0.02 (very bad) 

### Samsung vs. iPhone (GALS4A vs. IPHO7) validation

* Acceleration values:  IPHO7 has about 1/2 the noise level of GALS4A.
* Timing jitter:  Very similar, may be artefact of numerical precision.

# TODO

* More sophisticated resampling/filtering.
* Better app or device for measurement?  AccDataRec has a lot of timing jitter.  
  Should be OK for frequencies lower than 10Hz though.
  * Use VibSensor
* Try 
  <https://play.google.com/store/apps/details?id=com.chrystianvieyra.android.physicstoolboxaccelerometer>
* Measure closer to head.  Or phone mounted *on* head of test person.


# License and copyright

This project is (C) 2019 by KISS Technologies GmbH, Adliswil, Switzerland.  See
LICENSE for details.

# Acknowledgements

* Thanks to Davide and Gabriel for valuable discussions and input
* Thanks to the makers of AccDataRec and VibSensor


# References

* Wankkompensation im SBB FV-Dosto.  Stand der Versuche mit den beiden Prototypdrehgestellen 
  im Erprobungsträger.  Thomas Grossenbacher, Olten, 08.06.2011
<https://www.gdi-adi.ch/fileadmin/user_upload/downloads/ortsgruppen/mittelland_jura/de/110608_Thomas_Grossenbacher_WAKO.pdf>
* Wankkompensation im SBB FV-Dosto.  Stand der Versuche mit den beiden Prototypdrehgestellen 
  im Erprobungsträger.  Richard Schneider; Thomas Grossenbacher; 12. Sept. 2011
<http://www.schienenfahrzeugtagung.at/download/PDF2011/10-Schneider_Grossenbacher.pdf>
* Track slopes between Oerlikon and Altstetten.  <https://www.espazium.ch/uploads/MTQzODMyNzc1NC0zNjc1ODk1MTUwLTMwOTUtMzE=.jpg>
